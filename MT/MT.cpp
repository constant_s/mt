﻿#include "MT.h"

MT::MT()
{
	end = false;
	time = 0;
	limit = 5000;
	fields = gcnew array<String^>(5);
}
MT::MT(MT^ m)
{
	time = 0;
	limit = m->limit;
	stut = m->stut;
	end = false;
	fields = m->fields;
}
MT::MT(int s)
{
	time = 0;
	limit = 5000;
	stut = s;
	end = false;
	fields = gcnew array<String^>(5);
}

void MT::AddAlphList(String^ s)
{
	if (s->IsNullOrWhiteSpace(s))
		throw 1;
	alphabet = s->Split('\n');	
	for (int i = 0; i < alphabet->Length; i++)
		if (s->IsNullOrWhiteSpace(alphabet[i]) || alphabet[i]->Length > 1)
			throw 1;
	for (int i = 0; i < alphabet->Length - 1; i++)
		for (int j = i + 1; j < alphabet->Length; j++)
			if (alphabet[i] == alphabet[j])
				throw 1;
}
void MT::AddCondList(String^ s)
{
	if (s->IsNullOrWhiteSpace(s))
		throw 2;
	for (int i = 0; i < alphabet->Length; i++)
		for (int j = 0; j < s->Split('\n')->Length; j++)
			if (alphabet[i] == s->Split('\n')[j])
				throw 9;
	if (!s->IsNullOrWhiteSpace(s->Split('\n')[0]))
		cond = gcnew Condition(s->Split('\n')[0]);
	else throw 2;
	Condition^ c = cond;
	for (int i = 1; i < s->Split('\n')->Length; i++)
	{
		if (!s->IsNullOrWhiteSpace(s->Split('\n')[i]))
		{
			c->next = gcnew Condition(s->Split('\n')[i]);
			c = c->next;
		}
		else throw 2;
	}
	c = cond;
	bool cf = false;
	while (c != nullptr)
	{
		if (c->name == "END")
			cf = true;
		Condition^ cc = c->next;
		while (cc != nullptr)
		{
			if (c->name == cc->name)
				throw 2;
			cc = cc->next;
		}
		c = c->next;
	}
	if (!cf) throw 3;
}
void MT::AddCommList(String^ s)
{
	if (s->IsNullOrWhiteSpace(s))
		throw 4;
	array<String^>^ com = s->Split('\n');
	bool endf = false;
	for (int i = 0, j = 0; j < com->Length; i += 5, j++)
	{
		array<String^>^ comm;
		comm = com[j]->Split(' ');
		if (comm->Length != 6 || s->IsNullOrWhiteSpace(com[j]))
			throw 4;
		Condition^ c = cond;
		bool a = false, b = false;
		while (c != nullptr)
		{
			if (comm[0] == c->name && comm[0] != "END")
				a = true;
			if (comm[3] == c->name)
				b = true;
			c = c->next;
		}
		if (!b || !a) throw 4;
		a = false;
		b = false;
		for (int k = 0; k < alphabet->Length; k++)
		{
			if (alphabet[k] == comm[1])
				a = true;
			if (alphabet[k] == comm[4])
				b = true;
		}
		if (!b || !a) throw 4;
		c = cond;
		while (c != nullptr && c->name != comm[0])
			c = c->next;
		if (c != nullptr && c->name == comm[0])
			c->AddComm(comm[1][0], comm[4][0], comm[3], comm[5]);
		else throw "";
		if (comm[3] == "END")
			endf = true;
	}
	if (!endf) throw 5;
}
void MT::SetTape(String^ s)
{
	if (s->IsNullOrWhiteSpace(s))
		throw 8;
	for (int i = 0; i < s->Length; i++)
	{
		bool b = false;
		for (int j = 0; j < alphabet->Length; j++)
		{
			if (s[i] == alphabet[j][0])
				b = true;
		}
		if (!b) throw 8;
	}
	tape = gcnew Tape(s[0]);
	Tape^ tptr = tape;
	for (int i = 1; i < s->Length; i++)
	{
		tptr->next = gcnew Tape(s[i], tptr, i);
		tptr = tptr->next;
	}
}
void MT::GetTape(array<TextBox^>^ txtb, int m)
{
	Tape^ t = tape;
	if (t != nullptr && t->nomber != m)
	{
		if (t->nomber < m)
			while (t->next != nullptr && t->nomber != m)
				t = t->next;
		if (t->nomber > m)
			while (t->prev != nullptr && t->nomber != m)
				t = t->prev;
	}
	for (int i = 0; i < txtb->Length; i++)
	{
		if (t != nullptr && t->nomber == m)
		{
			txtb[i]->Text = t->value + "";
			t = t->next;
		}
		else
			txtb[i]->Text = L"λ";
		m++;
	}
}
void MT::SetStut(int i)
{
	stut = i;
}
int MT::GetStut()
{
	return stut;
}
void MT::SetCond(String^ s)
{
	if (s->IsNullOrWhiteSpace(s))
		throw 7;
	condition = s;
	if (s == "END")
		throw 6;
	Condition^ c = cond;
	bool b = false;
	while (c != nullptr)
	{
		if (s == c->name)
			b = true;
		c = c->next;
	}
	if (!b) throw 7;
} 
String^ MT::GetCond()
{
	return condition;
}
void MT::Step()
{
	Tape^ t = tape;
	Condition^ c = cond;
	while (t->nomber != stut)
	{
		if (t->nomber < stut)
		{
			if (t->next == nullptr)
				t->next = gcnew Tape(L'λ', t, t->nomber + 1);
			t = t->next;
		}
		if (t->nomber > stut)
		{
			if (t->prev == nullptr)
				t->prev = gcnew Tape(L'λ', t->nomber - 1, t);
			t = t->prev;
		}
	}
	while (c != nullptr && c->name != condition)
		c = c->next;
	if (c != nullptr && c->name == condition)
	{
		Command^ com = c->GetComm(t->value);
		if (com != nullptr)
		{
			pcom = com->value + " " + c->name + " -> " + com->next_value + " " + com->next_cond + " " + com->oper;
			t->value = com->next_value;
			condition = com->next_cond;
			if (com->oper == "R")
				stut++;
			if (com->oper == "L")
				stut--;
			if (condition == "END")
				end = true;
		}
	}
}
bool MT::GetEnd()
{
	time++;
	return end;
}
int MT::GetTime()
{
	return time;
}
bool MT::GetSEnd()
{
	if (time > limit)
	{
		end = true;
		throw 1;
	}
	time++;
	return end;
}
void MT::SetLimit(int l)
{
	limit = l;
}
int MT::GetLimit()
{
	return limit;
}
String^ MT::GetPCom()
{
	return pcom;
}
void MT::NewStart()
{
	time = 0;
	end = false;
}
void MT::SaveToFile(Object ^obj, String^ s)
{
	BinaryFormatter^ binF = gcnew BinaryFormatter();
	FileStream^ fStream = gcnew FileStream(s, FileMode::Create, FileAccess::Write, FileShare::None);
	fStream->Position = 0;
	binF->Serialize(fStream, obj);
	fStream->Close();
}
MT^ MT::LoadFromFile(String^ s)
{
	BinaryFormatter^ binF = gcnew BinaryFormatter();
	FileStream^ fStream = gcnew FileStream(s, FileMode::Open, FileAccess::Read, FileShare::None);
	fStream->Position = 0;
	MT^ mt = (MT^)binF->Deserialize(fStream);
	fStream->Close();
	return mt;
}
void MT::SetFields(array<String^>^ s)
{
	fields = s;
}
array<String^>^ MT::GetFields()
{
	return fields;
}