﻿#pragma once

#include "NewP.h"
#include "Exit.h"
#include "MT.h"
#include "Decor.h"
#include "About.h"

namespace TuringMachine {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Threading;
	using namespace System::Runtime::Serialization::Formatters::Binary;

public ref class MyForm : public System::Windows::Forms::Form
{
private:
	array<TextBox^>^ txtbox;
	MT^ mt;
	int value;
	int move;
	bool start = false;
	Decor^ cdw;
public: MyForm()
{
	cdw = gcnew Decor();
	InitializeComponent();
	txtbox = gcnew array<TextBox^>(18);
	txtbox[0] = textBox1;
	txtbox[1] = textBox2;
	txtbox[2] = textBox3;
	txtbox[3] = textBox4;
	txtbox[4] = textBox5;
	txtbox[5] = textBox6;
	txtbox[6] = textBox7;
	txtbox[7] = textBox8;
	txtbox[8] = textBox9;
	txtbox[9] = textBox10;
	txtbox[10] = textBox11;
	txtbox[11] = textBox12;
	txtbox[12] = textBox13;
	txtbox[13] = textBox14;
	txtbox[14] = textBox15;
	txtbox[15] = textBox16;
	txtbox[16] = textBox17;
	txtbox[17] = textBox18;
	value = 0;
	move = 0;
	for (int i = 0; i < txtbox->Length; i++)
	{
		txtbox[i]->BackColor = Color::White;
		txtbox[i]->Text = L"λ";
	}
	if (move + value >= 0 && move + value < 18)
		txtbox[value + move]->BackColor = Color::PaleTurquoise;
	mt = gcnew MT(value);
	startb->Enabled = false;
	step->Enabled = false;
	set->Focus();
	ChangeStut();
}
protected: ~MyForm()
{
	if (components)
	{
		delete components;
	}
}
private: System::Windows::Forms::ToolStripMenuItem^  aboutTheProgramToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  optionsToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  decorToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  executionLimitToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem2;
private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem3;
private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem4;
private: System::Windows::Forms::TextBox^  timer;
private: System::Windows::Forms::Label^  label9;
private: System::Windows::Forms::ToolStripMenuItem^  programToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  newProgramToolStripMenuItem1;
private: System::Windows::Forms::ToolStripMenuItem^  saveProgramToolStripMenuItem1;
private: System::Windows::Forms::ToolStripMenuItem^  loadProgramToolStripMenuItem1;
private: System::Windows::Forms::Button^  left;
private: System::Windows::Forms::Button^  right;
private: System::Windows::Forms::ToolStripMenuItem^  quitToolStripMenuItem;
private: System::Windows::Forms::SaveFileDialog^  saveFileDialog1;
private: System::Windows::Forms::OpenFileDialog^  openFileDialog1;
private: System::Windows::Forms::HelpProvider^  helpProvider1;
private: System::Windows::Forms::TextBox^  nowcond;
private: System::Windows::Forms::Label^  label8;
private: System::Windows::Forms::Label^  label7;
private: System::Windows::Forms::TextBox^  fcond;
private: System::Windows::Forms::TextBox^  comm;
private: System::Windows::Forms::Button^  set;
private: System::Windows::Forms::Label^  label6;
private: System::Windows::Forms::TextBox^  tapeTextBox;
private: System::Windows::Forms::RichTextBox^  commentTextBoxt;
private: System::Windows::Forms::TextBox^  textBox18;
private: System::Windows::Forms::TextBox^  textBox17;
private: System::Windows::Forms::TextBox^  textBox16;
private: System::Windows::Forms::TextBox^  textBox15;
private: System::Windows::Forms::TextBox^  textBox14;
private: System::Windows::Forms::TextBox^  textBox13;
private: System::Windows::Forms::TextBox^  textBox12;
private: System::Windows::Forms::TextBox^  textBox11;
private: System::Windows::Forms::TextBox^  textBox10;
private: System::Windows::Forms::TextBox^  textBox9;
private: System::Windows::Forms::TextBox^  textBox8;
private: System::Windows::Forms::TextBox^  textBox7;
private: System::Windows::Forms::TextBox^  textBox6;
private: System::Windows::Forms::TextBox^  textBox5;
private: System::Windows::Forms::TextBox^  textBox4;
private: System::Windows::Forms::TextBox^  textBox3;
private: System::Windows::Forms::TextBox^  textBox2;
private: System::Windows::Forms::TextBox^  textBox1;
private: System::Windows::Forms::Button^  startb;
private: System::Windows::Forms::Button^  step;
private: System::Windows::Forms::Label^  label5;
private: System::Windows::Forms::RichTextBox^  commTextBox;
private: System::Windows::Forms::Label^  label4;
private: System::Windows::Forms::RichTextBox^  alphTextBox;
private: System::Windows::Forms::Label^  label3;
private: System::Windows::Forms::RichTextBox^  condTextBox;
private: System::Windows::Forms::Label^  label2;
private: System::Windows::Forms::Label^  label1;
private: System::Windows::Forms::MenuStrip^  menuStrip1;
private:System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code

	void InitializeComponent(void)
			{
		this->set = (gcnew System::Windows::Forms::Button());
		this->label6 = (gcnew System::Windows::Forms::Label());
		this->tapeTextBox = (gcnew System::Windows::Forms::TextBox());
		this->left = (gcnew System::Windows::Forms::Button());
		this->right = (gcnew System::Windows::Forms::Button());
		this->commentTextBoxt = (gcnew System::Windows::Forms::RichTextBox());
		this->fcond = (gcnew System::Windows::Forms::TextBox());
		this->comm = (gcnew System::Windows::Forms::TextBox());
		this->textBox18 = (gcnew System::Windows::Forms::TextBox());
		this->textBox17 = (gcnew System::Windows::Forms::TextBox());
		this->textBox16 = (gcnew System::Windows::Forms::TextBox());
		this->textBox15 = (gcnew System::Windows::Forms::TextBox());
		this->textBox14 = (gcnew System::Windows::Forms::TextBox());
		this->textBox13 = (gcnew System::Windows::Forms::TextBox());
		this->textBox12 = (gcnew System::Windows::Forms::TextBox());
		this->textBox11 = (gcnew System::Windows::Forms::TextBox());
		this->textBox10 = (gcnew System::Windows::Forms::TextBox());
		this->textBox9 = (gcnew System::Windows::Forms::TextBox());
		this->textBox8 = (gcnew System::Windows::Forms::TextBox());
		this->textBox7 = (gcnew System::Windows::Forms::TextBox());
		this->textBox6 = (gcnew System::Windows::Forms::TextBox());
		this->textBox5 = (gcnew System::Windows::Forms::TextBox());
		this->textBox4 = (gcnew System::Windows::Forms::TextBox());
		this->textBox3 = (gcnew System::Windows::Forms::TextBox());
		this->textBox2 = (gcnew System::Windows::Forms::TextBox());
		this->textBox1 = (gcnew System::Windows::Forms::TextBox());
		this->startb = (gcnew System::Windows::Forms::Button());
		this->step = (gcnew System::Windows::Forms::Button());
		this->label5 = (gcnew System::Windows::Forms::Label());
		this->commTextBox = (gcnew System::Windows::Forms::RichTextBox());
		this->label4 = (gcnew System::Windows::Forms::Label());
		this->alphTextBox = (gcnew System::Windows::Forms::RichTextBox());
		this->label3 = (gcnew System::Windows::Forms::Label());
		this->condTextBox = (gcnew System::Windows::Forms::RichTextBox());
		this->label2 = (gcnew System::Windows::Forms::Label());
		this->label1 = (gcnew System::Windows::Forms::Label());
		this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
		this->programToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->newProgramToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->saveProgramToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->loadProgramToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->optionsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->decorToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->executionLimitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem2 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem3 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->toolStripMenuItem4 = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->aboutTheProgramToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->quitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
		this->label7 = (gcnew System::Windows::Forms::Label());
		this->saveFileDialog1 = (gcnew System::Windows::Forms::SaveFileDialog());
		this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
		this->helpProvider1 = (gcnew System::Windows::Forms::HelpProvider());
		this->nowcond = (gcnew System::Windows::Forms::TextBox());
		this->label8 = (gcnew System::Windows::Forms::Label());
		this->timer = (gcnew System::Windows::Forms::TextBox());
		this->label9 = (gcnew System::Windows::Forms::Label());
		this->menuStrip1->SuspendLayout();
		this->SuspendLayout();
		// 
		// set
		// 
		this->set->BackColor = System::Drawing::Color::White;
		this->set->Cursor = System::Windows::Forms::Cursors::Hand;
		this->set->FlatAppearance->BorderColor = System::Drawing::Color::White;
		this->set->FlatAppearance->BorderSize = 0;
		this->set->FlatAppearance->CheckedBackColor = System::Drawing::Color::White;
		this->set->FlatAppearance->MouseDownBackColor = System::Drawing::Color::DarkGray;
		this->set->FlatAppearance->MouseOverBackColor = System::Drawing::Color::Gainsboro;
		this->set->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
		this->set->Font = (gcnew System::Drawing::Font(L"Georgia", 16.2F));
		this->set->ForeColor = System::Drawing::Color::Black;
		this->helpProvider1->SetHelpNavigator(this->set, System::Windows::Forms::HelpNavigator::KeywordIndex);
		this->set->ImeMode = System::Windows::Forms::ImeMode::NoControl;
		this->set->Location = System::Drawing::Point(281, 61);
		this->set->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
		this->set->Name = L"set";
		this->helpProvider1->SetShowHelp(this->set, true);
		this->set->Size = System::Drawing::Size(175, 50);
		this->set->TabIndex = 119;
		this->set->Text = L"Setting";
		this->set->UseVisualStyleBackColor = false;
		this->set->Click += gcnew System::EventHandler(this, &MyForm::set_Click);
		// 
		// label6
		// 
		this->label6->AutoSize = true;
		this->label6->Font = (gcnew System::Drawing::Font(L"Georgia", 13));
		this->label6->ImeMode = System::Windows::Forms::ImeMode::NoControl;
		this->label6->Location = System::Drawing::Point(492, 351);
		this->label6->Name = L"label6";
		this->label6->Size = System::Drawing::Size(147, 27);
		this->label6->TabIndex = 118;
		this->label6->Text = L"Configuration";
		// 
		// tapeTextBox
		// 
		this->tapeTextBox->BackColor = System::Drawing::Color::White;
		this->tapeTextBox->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16.2F));
		this->tapeTextBox->Location = System::Drawing::Point(659, 343);
		this->tapeTextBox->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
		this->tapeTextBox->Name = L"tapeTextBox";
		this->tapeTextBox->ScrollBars = System::Windows::Forms::ScrollBars::Horizontal;
		this->helpProvider1->SetShowHelp(this->tapeTextBox, false);
		this->tapeTextBox->Size = System::Drawing::Size(428, 38);
		this->tapeTextBox->TabIndex = 117;
		this->tapeTextBox->Text = L"0010101010";
		// 
		// left
		// 
		this->left->BackColor = System::Drawing::Color::White;
		this->left->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Zoom;
		this->left->Cursor = System::Windows::Forms::Cursors::Hand;
		this->left->FlatAppearance->BorderColor = System::Drawing::Color::White;
		this->left->FlatAppearance->BorderSize = 0;
		this->left->FlatAppearance->CheckedBackColor = System::Drawing::Color::White;
		this->left->FlatAppearance->MouseDownBackColor = System::Drawing::Color::DarkGray;
		this->left->FlatAppearance->MouseOverBackColor = System::Drawing::Color::Gainsboro;
		this->left->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
		this->left->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(0)));
		this->left->ForeColor = System::Drawing::Color::Black;
		this->left->ImeMode = System::Windows::Forms::ImeMode::NoControl;
		this->left->Location = System::Drawing::Point(11, 142);
		this->left->Margin = System::Windows::Forms::Padding(2);
		this->left->Name = L"left";
		this->left->Size = System::Drawing::Size(50, 50);
		this->left->TabIndex = 116;
		this->left->Text = L"<";
		this->left->UseVisualStyleBackColor = false;
		this->left->Click += gcnew System::EventHandler(this, &MyForm::left_Click);
		// 
		// right
		// 
		this->right->BackColor = System::Drawing::Color::White;
		this->right->Cursor = System::Windows::Forms::Cursors::Hand;
		this->right->FlatAppearance->BorderColor = System::Drawing::Color::White;
		this->right->FlatAppearance->BorderSize = 0;
		this->right->FlatAppearance->CheckedBackColor = System::Drawing::Color::White;
		this->right->FlatAppearance->MouseDownBackColor = System::Drawing::Color::DarkGray;
		this->right->FlatAppearance->MouseOverBackColor = System::Drawing::Color::Gainsboro;
		this->right->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
		this->right->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(0)));
		this->right->ForeColor = System::Drawing::Color::Black;
		this->right->ImeMode = System::Windows::Forms::ImeMode::NoControl;
		this->right->Location = System::Drawing::Point(1037, 142);
		this->right->Margin = System::Windows::Forms::Padding(2);
		this->right->Name = L"right";
		this->right->Size = System::Drawing::Size(50, 50);
		this->right->TabIndex = 115;
		this->right->Text = L">";
		this->right->UseVisualStyleBackColor = false;
		this->right->Click += gcnew System::EventHandler(this, &MyForm::right_Click);
		// 
		// commentTextBoxt
		// 
		this->commentTextBoxt->Font = (gcnew System::Drawing::Font(L"Georgia", 13.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->commentTextBoxt->Location = System::Drawing::Point(497, 425);
		this->commentTextBoxt->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
		this->commentTextBoxt->Name = L"commentTextBoxt";
		this->commentTextBoxt->Size = System::Drawing::Size(592, 181);
		this->commentTextBoxt->TabIndex = 114;
		this->commentTextBoxt->Text = L"Replaces 0 to 1 and vice versa.";
		// 
		// fcond
		// 
		this->fcond->BackColor = System::Drawing::Color::White;
		this->fcond->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->fcond->Location = System::Drawing::Point(744, 225);
		this->fcond->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
		this->fcond->Name = L"fcond";
		this->fcond->Size = System::Drawing::Size(75, 30);
		this->fcond->TabIndex = 113;
		this->fcond->Text = L"q0";
		this->fcond->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		// 
		// comm
		// 
		this->comm->BackColor = System::Drawing::Color::White;
		this->comm->Cursor = System::Windows::Forms::Cursors::Default;
		this->comm->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->comm->Location = System::Drawing::Point(744, 302);
		this->comm->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
		this->comm->Name = L"comm";
		this->comm->ReadOnly = true;
		this->comm->Size = System::Drawing::Size(175, 30);
		this->comm->TabIndex = 112;
		this->comm->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		// 
		// textBox18
		// 
		this->textBox18->BackColor = System::Drawing::Color::White;
		this->textBox18->Cursor = System::Windows::Forms::Cursors::Hand;
		this->textBox18->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24));
		this->textBox18->ImeMode = System::Windows::Forms::ImeMode::Off;
		this->textBox18->Location = System::Drawing::Point(983, 142);
		this->textBox18->Margin = System::Windows::Forms::Padding(2);
		this->textBox18->Multiline = true;
		this->textBox18->Name = L"textBox18";
		this->textBox18->ReadOnly = true;
		this->helpProvider1->SetShowHelp(this->textBox18, false);
		this->textBox18->Size = System::Drawing::Size(50, 50);
		this->textBox18->TabIndex = 111;
		this->textBox18->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox18->WordWrap = false;
		this->textBox18->Click += gcnew System::EventHandler(this, &MyForm::textBox18_Click);
		this->textBox18->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::textBox18_MouseDown);
		// 
		// textBox17
		// 
		this->textBox17->BackColor = System::Drawing::Color::White;
		this->textBox17->Cursor = System::Windows::Forms::Cursors::Hand;
		this->textBox17->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24));
		this->textBox17->ImeMode = System::Windows::Forms::ImeMode::Off;
		this->textBox17->Location = System::Drawing::Point(929, 142);
		this->textBox17->Margin = System::Windows::Forms::Padding(2);
		this->textBox17->Multiline = true;
		this->textBox17->Name = L"textBox17";
		this->textBox17->ReadOnly = true;
		this->helpProvider1->SetShowHelp(this->textBox17, false);
		this->textBox17->Size = System::Drawing::Size(50, 50);
		this->textBox17->TabIndex = 110;
		this->textBox17->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox17->WordWrap = false;
		this->textBox17->Click += gcnew System::EventHandler(this, &MyForm::textBox17_Click);
		this->textBox17->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::textBox18_MouseDown);
		// 
		// textBox16
		// 
		this->textBox16->BackColor = System::Drawing::Color::White;
		this->textBox16->Cursor = System::Windows::Forms::Cursors::Hand;
		this->textBox16->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24));
		this->textBox16->ImeMode = System::Windows::Forms::ImeMode::Off;
		this->textBox16->Location = System::Drawing::Point(875, 142);
		this->textBox16->Margin = System::Windows::Forms::Padding(2);
		this->textBox16->Multiline = true;
		this->textBox16->Name = L"textBox16";
		this->textBox16->ReadOnly = true;
		this->helpProvider1->SetShowHelp(this->textBox16, false);
		this->textBox16->Size = System::Drawing::Size(50, 50);
		this->textBox16->TabIndex = 109;
		this->textBox16->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox16->WordWrap = false;
		this->textBox16->Click += gcnew System::EventHandler(this, &MyForm::textBox16_Click);
		this->textBox16->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::textBox18_MouseDown);
		// 
		// textBox15
		// 
		this->textBox15->BackColor = System::Drawing::Color::White;
		this->textBox15->Cursor = System::Windows::Forms::Cursors::Hand;
		this->textBox15->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24));
		this->textBox15->ImeMode = System::Windows::Forms::ImeMode::Off;
		this->textBox15->Location = System::Drawing::Point(821, 142);
		this->textBox15->Margin = System::Windows::Forms::Padding(2);
		this->textBox15->Multiline = true;
		this->textBox15->Name = L"textBox15";
		this->textBox15->ReadOnly = true;
		this->helpProvider1->SetShowHelp(this->textBox15, false);
		this->textBox15->Size = System::Drawing::Size(50, 50);
		this->textBox15->TabIndex = 108;
		this->textBox15->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox15->WordWrap = false;
		this->textBox15->Click += gcnew System::EventHandler(this, &MyForm::textBox15_Click);
		this->textBox15->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::textBox18_MouseDown);
		// 
		// textBox14
		// 
		this->textBox14->BackColor = System::Drawing::Color::White;
		this->textBox14->Cursor = System::Windows::Forms::Cursors::Hand;
		this->textBox14->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24));
		this->textBox14->ImeMode = System::Windows::Forms::ImeMode::Off;
		this->textBox14->Location = System::Drawing::Point(767, 142);
		this->textBox14->Margin = System::Windows::Forms::Padding(2);
		this->textBox14->Multiline = true;
		this->textBox14->Name = L"textBox14";
		this->textBox14->ReadOnly = true;
		this->helpProvider1->SetShowHelp(this->textBox14, false);
		this->textBox14->Size = System::Drawing::Size(50, 50);
		this->textBox14->TabIndex = 107;
		this->textBox14->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox14->WordWrap = false;
		this->textBox14->Click += gcnew System::EventHandler(this, &MyForm::textBox14_Click);
		this->textBox14->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::textBox18_MouseDown);
		// 
		// textBox13
		// 
		this->textBox13->BackColor = System::Drawing::Color::White;
		this->textBox13->Cursor = System::Windows::Forms::Cursors::Hand;
		this->textBox13->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24));
		this->textBox13->ImeMode = System::Windows::Forms::ImeMode::Off;
		this->textBox13->Location = System::Drawing::Point(713, 142);
		this->textBox13->Margin = System::Windows::Forms::Padding(2);
		this->textBox13->Multiline = true;
		this->textBox13->Name = L"textBox13";
		this->textBox13->ReadOnly = true;
		this->helpProvider1->SetShowHelp(this->textBox13, false);
		this->textBox13->Size = System::Drawing::Size(50, 50);
		this->textBox13->TabIndex = 106;
		this->textBox13->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox13->WordWrap = false;
		this->textBox13->Click += gcnew System::EventHandler(this, &MyForm::textBox13_Click);
		this->textBox13->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::textBox18_MouseDown);
		// 
		// textBox12
		// 
		this->textBox12->BackColor = System::Drawing::Color::White;
		this->textBox12->Cursor = System::Windows::Forms::Cursors::Hand;
		this->textBox12->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24));
		this->textBox12->ImeMode = System::Windows::Forms::ImeMode::Off;
		this->textBox12->Location = System::Drawing::Point(659, 142);
		this->textBox12->Margin = System::Windows::Forms::Padding(2);
		this->textBox12->Multiline = true;
		this->textBox12->Name = L"textBox12";
		this->textBox12->ReadOnly = true;
		this->helpProvider1->SetShowHelp(this->textBox12, false);
		this->textBox12->Size = System::Drawing::Size(50, 50);
		this->textBox12->TabIndex = 105;
		this->textBox12->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox12->WordWrap = false;
		this->textBox12->Click += gcnew System::EventHandler(this, &MyForm::textBox12_Click);
		this->textBox12->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::textBox18_MouseDown);
		// 
		// textBox11
		// 
		this->textBox11->BackColor = System::Drawing::Color::White;
		this->textBox11->Cursor = System::Windows::Forms::Cursors::Hand;
		this->textBox11->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24));
		this->textBox11->ImeMode = System::Windows::Forms::ImeMode::Off;
		this->textBox11->Location = System::Drawing::Point(605, 142);
		this->textBox11->Margin = System::Windows::Forms::Padding(2);
		this->textBox11->Multiline = true;
		this->textBox11->Name = L"textBox11";
		this->textBox11->ReadOnly = true;
		this->helpProvider1->SetShowHelp(this->textBox11, false);
		this->textBox11->Size = System::Drawing::Size(50, 50);
		this->textBox11->TabIndex = 104;
		this->textBox11->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox11->WordWrap = false;
		this->textBox11->Click += gcnew System::EventHandler(this, &MyForm::textBox11_Click);
		this->textBox11->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::textBox18_MouseDown);
		// 
		// textBox10
		// 
		this->textBox10->BackColor = System::Drawing::Color::White;
		this->textBox10->Cursor = System::Windows::Forms::Cursors::Hand;
		this->textBox10->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24));
		this->textBox10->ImeMode = System::Windows::Forms::ImeMode::Off;
		this->textBox10->Location = System::Drawing::Point(551, 142);
		this->textBox10->Margin = System::Windows::Forms::Padding(2);
		this->textBox10->Multiline = true;
		this->textBox10->Name = L"textBox10";
		this->textBox10->ReadOnly = true;
		this->helpProvider1->SetShowHelp(this->textBox10, false);
		this->textBox10->Size = System::Drawing::Size(50, 50);
		this->textBox10->TabIndex = 103;
		this->textBox10->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox10->WordWrap = false;
		this->textBox10->Click += gcnew System::EventHandler(this, &MyForm::textBox10_Click);
		this->textBox10->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::textBox18_MouseDown);
		// 
		// textBox9
		// 
		this->textBox9->BackColor = System::Drawing::Color::White;
		this->textBox9->Cursor = System::Windows::Forms::Cursors::Hand;
		this->textBox9->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24));
		this->textBox9->ImeMode = System::Windows::Forms::ImeMode::Off;
		this->textBox9->Location = System::Drawing::Point(497, 142);
		this->textBox9->Margin = System::Windows::Forms::Padding(2);
		this->textBox9->Multiline = true;
		this->textBox9->Name = L"textBox9";
		this->textBox9->ReadOnly = true;
		this->helpProvider1->SetShowHelp(this->textBox9, false);
		this->textBox9->Size = System::Drawing::Size(50, 50);
		this->textBox9->TabIndex = 102;
		this->textBox9->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox9->WordWrap = false;
		this->textBox9->Click += gcnew System::EventHandler(this, &MyForm::textBox9_Click);
		this->textBox9->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::textBox18_MouseDown);
		// 
		// textBox8
		// 
		this->textBox8->BackColor = System::Drawing::Color::White;
		this->textBox8->Cursor = System::Windows::Forms::Cursors::Hand;
		this->textBox8->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24));
		this->textBox8->ImeMode = System::Windows::Forms::ImeMode::Off;
		this->textBox8->Location = System::Drawing::Point(443, 142);
		this->textBox8->Margin = System::Windows::Forms::Padding(2);
		this->textBox8->Multiline = true;
		this->textBox8->Name = L"textBox8";
		this->textBox8->ReadOnly = true;
		this->helpProvider1->SetShowHelp(this->textBox8, false);
		this->textBox8->Size = System::Drawing::Size(50, 50);
		this->textBox8->TabIndex = 101;
		this->textBox8->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox8->WordWrap = false;
		this->textBox8->Click += gcnew System::EventHandler(this, &MyForm::textBox8_Click);
		this->textBox8->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::textBox18_MouseDown);
		// 
		// textBox7
		// 
		this->textBox7->BackColor = System::Drawing::Color::White;
		this->textBox7->Cursor = System::Windows::Forms::Cursors::Hand;
		this->textBox7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24));
		this->textBox7->ImeMode = System::Windows::Forms::ImeMode::Off;
		this->textBox7->Location = System::Drawing::Point(389, 142);
		this->textBox7->Margin = System::Windows::Forms::Padding(2);
		this->textBox7->Multiline = true;
		this->textBox7->Name = L"textBox7";
		this->textBox7->ReadOnly = true;
		this->helpProvider1->SetShowHelp(this->textBox7, false);
		this->textBox7->Size = System::Drawing::Size(50, 50);
		this->textBox7->TabIndex = 100;
		this->textBox7->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox7->WordWrap = false;
		this->textBox7->Click += gcnew System::EventHandler(this, &MyForm::textBox7_Click);
		this->textBox7->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::textBox18_MouseDown);
		// 
		// textBox6
		// 
		this->textBox6->BackColor = System::Drawing::Color::White;
		this->textBox6->Cursor = System::Windows::Forms::Cursors::Hand;
		this->textBox6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24));
		this->textBox6->ImeMode = System::Windows::Forms::ImeMode::Off;
		this->textBox6->Location = System::Drawing::Point(335, 142);
		this->textBox6->Margin = System::Windows::Forms::Padding(2);
		this->textBox6->Multiline = true;
		this->textBox6->Name = L"textBox6";
		this->textBox6->ReadOnly = true;
		this->helpProvider1->SetShowHelp(this->textBox6, false);
		this->textBox6->Size = System::Drawing::Size(50, 50);
		this->textBox6->TabIndex = 99;
		this->textBox6->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox6->WordWrap = false;
		this->textBox6->Click += gcnew System::EventHandler(this, &MyForm::textBox6_Click);
		this->textBox6->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::textBox18_MouseDown);
		// 
		// textBox5
		// 
		this->textBox5->BackColor = System::Drawing::Color::White;
		this->textBox5->Cursor = System::Windows::Forms::Cursors::Hand;
		this->textBox5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24));
		this->textBox5->ImeMode = System::Windows::Forms::ImeMode::Off;
		this->textBox5->Location = System::Drawing::Point(281, 142);
		this->textBox5->Margin = System::Windows::Forms::Padding(2);
		this->textBox5->Multiline = true;
		this->textBox5->Name = L"textBox5";
		this->textBox5->ReadOnly = true;
		this->helpProvider1->SetShowHelp(this->textBox5, false);
		this->textBox5->Size = System::Drawing::Size(50, 50);
		this->textBox5->TabIndex = 98;
		this->textBox5->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox5->WordWrap = false;
		this->textBox5->Click += gcnew System::EventHandler(this, &MyForm::textBox5_Click);
		this->textBox5->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::textBox18_MouseDown);
		// 
		// textBox4
		// 
		this->textBox4->BackColor = System::Drawing::Color::White;
		this->textBox4->Cursor = System::Windows::Forms::Cursors::Hand;
		this->textBox4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24));
		this->textBox4->ImeMode = System::Windows::Forms::ImeMode::Off;
		this->textBox4->Location = System::Drawing::Point(227, 142);
		this->textBox4->Margin = System::Windows::Forms::Padding(2);
		this->textBox4->Multiline = true;
		this->textBox4->Name = L"textBox4";
		this->textBox4->ReadOnly = true;
		this->helpProvider1->SetShowHelp(this->textBox4, false);
		this->textBox4->Size = System::Drawing::Size(50, 50);
		this->textBox4->TabIndex = 97;
		this->textBox4->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox4->WordWrap = false;
		this->textBox4->Click += gcnew System::EventHandler(this, &MyForm::textBox4_Click);
		this->textBox4->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::textBox18_MouseDown);
		// 
		// textBox3
		// 
		this->textBox3->BackColor = System::Drawing::Color::White;
		this->textBox3->Cursor = System::Windows::Forms::Cursors::Hand;
		this->textBox3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24));
		this->textBox3->ImeMode = System::Windows::Forms::ImeMode::Off;
		this->textBox3->Location = System::Drawing::Point(173, 142);
		this->textBox3->Margin = System::Windows::Forms::Padding(2);
		this->textBox3->Multiline = true;
		this->textBox3->Name = L"textBox3";
		this->textBox3->ReadOnly = true;
		this->helpProvider1->SetShowHelp(this->textBox3, false);
		this->textBox3->Size = System::Drawing::Size(50, 50);
		this->textBox3->TabIndex = 96;
		this->textBox3->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox3->WordWrap = false;
		this->textBox3->Click += gcnew System::EventHandler(this, &MyForm::textBox3_Click);
		this->textBox3->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::textBox18_MouseDown);
		// 
		// textBox2
		// 
		this->textBox2->BackColor = System::Drawing::Color::White;
		this->textBox2->Cursor = System::Windows::Forms::Cursors::Hand;
		this->textBox2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24));
		this->textBox2->ImeMode = System::Windows::Forms::ImeMode::Off;
		this->textBox2->Location = System::Drawing::Point(119, 142);
		this->textBox2->Margin = System::Windows::Forms::Padding(2);
		this->textBox2->Multiline = true;
		this->textBox2->Name = L"textBox2";
		this->textBox2->ReadOnly = true;
		this->helpProvider1->SetShowHelp(this->textBox2, false);
		this->textBox2->Size = System::Drawing::Size(50, 50);
		this->textBox2->TabIndex = 95;
		this->textBox2->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox2->WordWrap = false;
		this->textBox2->Click += gcnew System::EventHandler(this, &MyForm::textBox2_Click);
		this->textBox2->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::textBox18_MouseDown);
		// 
		// textBox1
		// 
		this->textBox1->BackColor = System::Drawing::Color::White;
		this->textBox1->Cursor = System::Windows::Forms::Cursors::Hand;
		this->textBox1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24));
		this->textBox1->ImeMode = System::Windows::Forms::ImeMode::Off;
		this->textBox1->Location = System::Drawing::Point(65, 142);
		this->textBox1->Margin = System::Windows::Forms::Padding(2);
		this->textBox1->Multiline = true;
		this->textBox1->Name = L"textBox1";
		this->textBox1->ReadOnly = true;
		this->helpProvider1->SetShowHelp(this->textBox1, false);
		this->textBox1->Size = System::Drawing::Size(50, 50);
		this->textBox1->TabIndex = 94;
		this->textBox1->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->textBox1->WordWrap = false;
		this->textBox1->Click += gcnew System::EventHandler(this, &MyForm::textBox1_Click);
		this->textBox1->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::textBox18_MouseDown);
		// 
		// startb
		// 
		this->startb->BackColor = System::Drawing::Color::White;
		this->startb->Cursor = System::Windows::Forms::Cursors::Hand;
		this->startb->FlatAppearance->BorderColor = System::Drawing::Color::White;
		this->startb->FlatAppearance->BorderSize = 0;
		this->startb->FlatAppearance->CheckedBackColor = System::Drawing::Color::White;
		this->startb->FlatAppearance->MouseDownBackColor = System::Drawing::Color::DarkGray;
		this->startb->FlatAppearance->MouseOverBackColor = System::Drawing::Color::Gainsboro;
		this->startb->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
		this->startb->Font = (gcnew System::Drawing::Font(L"Georgia", 16.2F));
		this->startb->ForeColor = System::Drawing::Color::Black;
		this->helpProvider1->SetHelpNavigator(this->startb, System::Windows::Forms::HelpNavigator::Index);
		this->startb->ImeMode = System::Windows::Forms::ImeMode::NoControl;
		this->startb->Location = System::Drawing::Point(644, 61);
		this->startb->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
		this->startb->Name = L"startb";
		this->helpProvider1->SetShowHelp(this->startb, true);
		this->startb->Size = System::Drawing::Size(175, 50);
		this->startb->TabIndex = 92;
		this->startb->Text = L"Start";
		this->startb->UseVisualStyleBackColor = false;
		this->startb->Click += gcnew System::EventHandler(this, &MyForm::start_Click);
		// 
		// step
		// 
		this->step->BackColor = System::Drawing::Color::White;
		this->step->Cursor = System::Windows::Forms::Cursors::Hand;
		this->step->FlatAppearance->BorderColor = System::Drawing::Color::White;
		this->step->FlatAppearance->BorderSize = 0;
		this->step->FlatAppearance->CheckedBackColor = System::Drawing::Color::White;
		this->step->FlatAppearance->MouseDownBackColor = System::Drawing::Color::DarkGray;
		this->step->FlatAppearance->MouseOverBackColor = System::Drawing::Color::Gainsboro;
		this->step->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
		this->step->Font = (gcnew System::Drawing::Font(L"Georgia", 16.2F));
		this->step->ForeColor = System::Drawing::Color::Black;
		this->helpProvider1->SetHelpNavigator(this->step, System::Windows::Forms::HelpNavigator::TopicId);
		this->step->ImeMode = System::Windows::Forms::ImeMode::NoControl;
		this->step->Location = System::Drawing::Point(462, 61);
		this->step->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
		this->step->Name = L"step";
		this->helpProvider1->SetShowHelp(this->step, true);
		this->step->Size = System::Drawing::Size(175, 50);
		this->step->TabIndex = 91;
		this->step->Text = L"Step";
		this->step->UseVisualStyleBackColor = false;
		this->step->Click += gcnew System::EventHandler(this, &MyForm::step_Click);
		// 
		// label5
		// 
		this->label5->AutoSize = true;
		this->label5->Font = (gcnew System::Drawing::Font(L"Georgia", 13));
		this->label5->ImeMode = System::Windows::Forms::ImeMode::NoControl;
		this->label5->Location = System::Drawing::Point(276, 222);
		this->label5->Name = L"label5";
		this->label5->Size = System::Drawing::Size(123, 27);
		this->label5->TabIndex = 89;
		this->label5->Text = L"Commands";
		// 
		// commTextBox
		// 
		this->commTextBox->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->commTextBox->Location = System::Drawing::Point(270, 268);
		this->commTextBox->Margin = System::Windows::Forms::Padding(5, 2, 5, 2);
		this->commTextBox->Name = L"commTextBox";
		this->commTextBox->ScrollBars = System::Windows::Forms::RichTextBoxScrollBars::Vertical;
		this->commTextBox->Size = System::Drawing::Size(195, 338);
		this->commTextBox->TabIndex = 88;
		this->commTextBox->Text = L"q0 1 -> q0 0 R\nq0 0 -> q0 1 R\nq0 λ -> END λ E";
		// 
		// label4
		// 
		this->label4->AutoSize = true;
		this->label4->Font = (gcnew System::Drawing::Font(L"Georgia", 13));
		this->label4->ImeMode = System::Windows::Forms::ImeMode::NoControl;
		this->label4->Location = System::Drawing::Point(154, 222);
		this->label4->Name = L"label4";
		this->label4->Size = System::Drawing::Size(101, 27);
		this->label4->TabIndex = 87;
		this->label4->Text = L"Alphabet";
		// 
		// alphTextBox
		// 
		this->alphTextBox->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
		this->alphTextBox->Location = System::Drawing::Point(159, 269);
		this->alphTextBox->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
		this->alphTextBox->Name = L"alphTextBox";
		this->alphTextBox->ScrollBars = System::Windows::Forms::RichTextBoxScrollBars::Vertical;
		this->alphTextBox->Size = System::Drawing::Size(80, 337);
		this->alphTextBox->TabIndex = 86;
		this->alphTextBox->Text = L"λ\n1\n0";
		// 
		// label3
		// 
		this->label3->AutoSize = true;
		this->label3->Font = (gcnew System::Drawing::Font(L"Georgia", 13));
		this->label3->ImeMode = System::Windows::Forms::ImeMode::NoControl;
		this->label3->Location = System::Drawing::Point(14, 222);
		this->label3->Name = L"label3";
		this->label3->Size = System::Drawing::Size(119, 27);
		this->label3->TabIndex = 85;
		this->label3->Text = L"Conditions";
		// 
		// condTextBox
		// 
		this->condTextBox->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
		this->condTextBox->Location = System::Drawing::Point(35, 268);
		this->condTextBox->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
		this->condTextBox->Name = L"condTextBox";
		this->condTextBox->ScrollBars = System::Windows::Forms::RichTextBoxScrollBars::Vertical;
		this->condTextBox->Size = System::Drawing::Size(80, 337);
		this->condTextBox->TabIndex = 84;
		this->condTextBox->Text = L"END\nq0";
		// 
		// label2
		// 
		this->label2->AutoSize = true;
		this->label2->Font = (gcnew System::Drawing::Font(L"Georgia", 13));
		this->label2->ImeMode = System::Windows::Forms::ImeMode::NoControl;
		this->label2->Location = System::Drawing::Point(492, 227);
		this->label2->Name = L"label2";
		this->label2->Size = System::Drawing::Size(156, 27);
		this->label2->TabIndex = 83;
		this->label2->Text = L"First condition";
		// 
		// label1
		// 
		this->label1->AutoSize = true;
		this->label1->Font = (gcnew System::Drawing::Font(L"Georgia", 13));
		this->label1->ImeMode = System::Windows::Forms::ImeMode::NoControl;
		this->label1->Location = System::Drawing::Point(492, 304);
		this->label1->Margin = System::Windows::Forms::Padding(3, 10, 3, 10);
		this->label1->Name = L"label1";
		this->label1->Size = System::Drawing::Size(217, 27);
		this->label1->TabIndex = 82;
		this->label1->Text = L"Perfomanse commad";
		// 
		// menuStrip1
		// 
		this->menuStrip1->BackColor = System::Drawing::Color::WhiteSmoke;
		this->menuStrip1->Font = (gcnew System::Drawing::Font(L"Georgia", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->menuStrip1->ImageScalingSize = System::Drawing::Size(20, 20);
		this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {
			this->programToolStripMenuItem,
				this->optionsToolStripMenuItem, this->aboutTheProgramToolStripMenuItem, this->quitToolStripMenuItem
		});
		this->menuStrip1->Location = System::Drawing::Point(0, 0);
		this->menuStrip1->Name = L"menuStrip1";
		this->menuStrip1->Padding = System::Windows::Forms::Padding(3, 2, 0, 2);
		this->menuStrip1->RenderMode = System::Windows::Forms::ToolStripRenderMode::Professional;
		this->helpProvider1->SetShowHelp(this->menuStrip1, true);
		this->menuStrip1->ShowItemToolTips = true;
		this->menuStrip1->Size = System::Drawing::Size(1098, 32);
		this->menuStrip1->TabIndex = 90;
		this->menuStrip1->Text = L"menuStrip1";
		// 
		// programToolStripMenuItem
		// 
		this->programToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
			this->newProgramToolStripMenuItem1,
				this->saveProgramToolStripMenuItem1, this->loadProgramToolStripMenuItem1
		});
		this->programToolStripMenuItem->Name = L"programToolStripMenuItem";
		this->programToolStripMenuItem->Size = System::Drawing::Size(99, 28);
		this->programToolStripMenuItem->Text = L"Program";
		// 
		// newProgramToolStripMenuItem1
		// 
		this->newProgramToolStripMenuItem1->BackColor = System::Drawing::Color::White;
		this->newProgramToolStripMenuItem1->Name = L"newProgramToolStripMenuItem1";
		this->newProgramToolStripMenuItem1->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::N));
		this->newProgramToolStripMenuItem1->Size = System::Drawing::Size(283, 28);
		this->newProgramToolStripMenuItem1->Text = L"New program  ";
		this->newProgramToolStripMenuItem1->Click += gcnew System::EventHandler(this, &MyForm::newProgramToolStripMenuItem_Click);
		// 
		// saveProgramToolStripMenuItem1
		// 
		this->saveProgramToolStripMenuItem1->BackColor = System::Drawing::Color::White;
		this->saveProgramToolStripMenuItem1->Name = L"saveProgramToolStripMenuItem1";
		this->saveProgramToolStripMenuItem1->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::S));
		this->saveProgramToolStripMenuItem1->Size = System::Drawing::Size(283, 28);
		this->saveProgramToolStripMenuItem1->Text = L"Save program";
		this->saveProgramToolStripMenuItem1->Click += gcnew System::EventHandler(this, &MyForm::saveProgramToolStripMenuItem_Click);
		// 
		// loadProgramToolStripMenuItem1
		// 
		this->loadProgramToolStripMenuItem1->BackColor = System::Drawing::Color::White;
		this->loadProgramToolStripMenuItem1->Name = L"loadProgramToolStripMenuItem1";
		this->loadProgramToolStripMenuItem1->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::L));
		this->loadProgramToolStripMenuItem1->Size = System::Drawing::Size(283, 28);
		this->loadProgramToolStripMenuItem1->Text = L"Load program";
		this->loadProgramToolStripMenuItem1->Click += gcnew System::EventHandler(this, &MyForm::loadProgramToolStripMenuItem_Click);
		// 
		// optionsToolStripMenuItem
		// 
		this->optionsToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
			this->decorToolStripMenuItem,
				this->executionLimitToolStripMenuItem
		});
		this->optionsToolStripMenuItem->ForeColor = System::Drawing::Color::Black;
		this->optionsToolStripMenuItem->Name = L"optionsToolStripMenuItem";
		this->optionsToolStripMenuItem->Size = System::Drawing::Size(93, 28);
		this->optionsToolStripMenuItem->Text = L"Options";
		// 
		// decorToolStripMenuItem
		// 
		this->decorToolStripMenuItem->BackColor = System::Drawing::Color::White;
		this->decorToolStripMenuItem->Name = L"decorToolStripMenuItem";
		this->decorToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::D));
		this->decorToolStripMenuItem->Size = System::Drawing::Size(223, 28);
		this->decorToolStripMenuItem->Text = L"Decor";
		this->decorToolStripMenuItem->TextImageRelation = System::Windows::Forms::TextImageRelation::ImageAboveText;
		this->decorToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::decorToolStripMenuItem_Click);
		// 
		// executionLimitToolStripMenuItem
		// 
		this->executionLimitToolStripMenuItem->BackColor = System::Drawing::Color::White;
		this->executionLimitToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
			this->toolStripMenuItem2,
				this->toolStripMenuItem3, this->toolStripMenuItem4
		});
		this->executionLimitToolStripMenuItem->Name = L"executionLimitToolStripMenuItem";
		this->executionLimitToolStripMenuItem->Size = System::Drawing::Size(223, 28);
		this->executionLimitToolStripMenuItem->Text = L"Execution limit ";
		this->executionLimitToolStripMenuItem->TextImageRelation = System::Windows::Forms::TextImageRelation::ImageAboveText;
		// 
		// toolStripMenuItem2
		// 
		this->toolStripMenuItem2->BackColor = System::Drawing::Color::White;
		this->toolStripMenuItem2->CheckOnClick = true;
		this->toolStripMenuItem2->Name = L"toolStripMenuItem2";
		this->toolStripMenuItem2->Size = System::Drawing::Size(139, 28);
		this->toolStripMenuItem2->Text = L"500";
		this->toolStripMenuItem2->Click += gcnew System::EventHandler(this, &MyForm::toolStripMenuItem2_Click);
		// 
		// toolStripMenuItem3
		// 
		this->toolStripMenuItem3->BackColor = System::Drawing::Color::White;
		this->toolStripMenuItem3->Checked = true;
		this->toolStripMenuItem3->CheckOnClick = true;
		this->toolStripMenuItem3->CheckState = System::Windows::Forms::CheckState::Checked;
		this->toolStripMenuItem3->Name = L"toolStripMenuItem3";
		this->toolStripMenuItem3->Size = System::Drawing::Size(139, 28);
		this->toolStripMenuItem3->Text = L"5000";
		this->toolStripMenuItem3->Click += gcnew System::EventHandler(this, &MyForm::toolStripMenuItem3_Click);
		// 
		// toolStripMenuItem4
		// 
		this->toolStripMenuItem4->BackColor = System::Drawing::Color::White;
		this->toolStripMenuItem4->CheckOnClick = true;
		this->toolStripMenuItem4->Name = L"toolStripMenuItem4";
		this->toolStripMenuItem4->Size = System::Drawing::Size(139, 28);
		this->toolStripMenuItem4->Text = L"50000";
		this->toolStripMenuItem4->Click += gcnew System::EventHandler(this, &MyForm::toolStripMenuItem4_Click);
		// 
		// aboutTheProgramToolStripMenuItem
		// 
		this->aboutTheProgramToolStripMenuItem->Name = L"aboutTheProgramToolStripMenuItem";
		this->aboutTheProgramToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::A));
		this->aboutTheProgramToolStripMenuItem->Size = System::Drawing::Size(76, 28);
		this->aboutTheProgramToolStripMenuItem->Text = L"About";
		this->aboutTheProgramToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::aboutTheProgramToolStripMenuItem_Click);
		// 
		// quitToolStripMenuItem
		// 
		this->quitToolStripMenuItem->ForeColor = System::Drawing::Color::Black;
		this->quitToolStripMenuItem->Name = L"quitToolStripMenuItem";
		this->quitToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::Q));
		this->quitToolStripMenuItem->Size = System::Drawing::Size(62, 28);
		this->quitToolStripMenuItem->Text = L"Quit";
		this->quitToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::quitToolStripMenuItem_Click);
		// 
		// label7
		// 
		this->label7->AutoSize = true;
		this->label7->Font = (gcnew System::Drawing::Font(L"Georgia", 13));
		this->label7->ImeMode = System::Windows::Forms::ImeMode::NoControl;
		this->label7->Location = System::Drawing::Point(492, 396);
		this->label7->Margin = System::Windows::Forms::Padding(5, 0, 5, 0);
		this->label7->Name = L"label7";
		this->label7->Size = System::Drawing::Size(118, 27);
		this->label7->TabIndex = 120;
		this->label7->Text = L"Comments";
		// 
		// openFileDialog1
		// 
		this->openFileDialog1->FileName = L"openFileDialog1";
		// 
		// nowcond
		// 
		this->nowcond->BackColor = System::Drawing::Color::White;
		this->nowcond->Cursor = System::Windows::Forms::Cursors::Arrow;
		this->nowcond->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->nowcond->Location = System::Drawing::Point(744, 265);
		this->nowcond->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
		this->nowcond->Name = L"nowcond";
		this->nowcond->ReadOnly = true;
		this->nowcond->Size = System::Drawing::Size(75, 30);
		this->nowcond->TabIndex = 122;
		this->nowcond->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		// 
		// label8
		// 
		this->label8->AutoSize = true;
		this->label8->Font = (gcnew System::Drawing::Font(L"Georgia", 13));
		this->label8->ImeMode = System::Windows::Forms::ImeMode::NoControl;
		this->label8->Location = System::Drawing::Point(492, 267);
		this->label8->Name = L"label8";
		this->label8->Size = System::Drawing::Size(185, 27);
		this->label8->TabIndex = 123;
		this->label8->Text = L"Present condition";
		// 
		// timer
		// 
		this->timer->BackColor = System::Drawing::Color::White;
		this->timer->Cursor = System::Windows::Forms::Cursors::Arrow;
		this->timer->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->timer->Location = System::Drawing::Point(1002, 239);
		this->timer->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
		this->timer->Name = L"timer";
		this->timer->ReadOnly = true;
		this->timer->Size = System::Drawing::Size(75, 30);
		this->timer->TabIndex = 124;
		this->timer->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->timer->Visible = false;
		// 
		// label9
		// 
		this->label9->AutoSize = true;
		this->label9->Font = (gcnew System::Drawing::Font(L"Georgia", 13));
		this->label9->ImeMode = System::Windows::Forms::ImeMode::NoControl;
		this->label9->Location = System::Drawing::Point(847, 227);
		this->label9->Name = L"label9";
		this->label9->Size = System::Drawing::Size(132, 54);
		this->label9->TabIndex = 125;
		this->label9->Text = L"The number\r\nof passes";
		this->label9->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
		this->label9->Visible = false;
		// 
		// MyForm
		// 
		this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
		this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
		this->BackColor = System::Drawing::Color::White;
		this->ClientSize = System::Drawing::Size(1098, 623);
		this->Controls->Add(this->label9);
		this->Controls->Add(this->timer);
		this->Controls->Add(this->label8);
		this->Controls->Add(this->nowcond);
		this->Controls->Add(this->label7);
		this->Controls->Add(this->set);
		this->Controls->Add(this->label6);
		this->Controls->Add(this->tapeTextBox);
		this->Controls->Add(this->left);
		this->Controls->Add(this->right);
		this->Controls->Add(this->commentTextBoxt);
		this->Controls->Add(this->fcond);
		this->Controls->Add(this->comm);
		this->Controls->Add(this->textBox18);
		this->Controls->Add(this->textBox17);
		this->Controls->Add(this->textBox16);
		this->Controls->Add(this->textBox15);
		this->Controls->Add(this->textBox14);
		this->Controls->Add(this->textBox13);
		this->Controls->Add(this->textBox12);
		this->Controls->Add(this->textBox11);
		this->Controls->Add(this->textBox10);
		this->Controls->Add(this->textBox9);
		this->Controls->Add(this->textBox8);
		this->Controls->Add(this->textBox7);
		this->Controls->Add(this->textBox6);
		this->Controls->Add(this->textBox5);
		this->Controls->Add(this->textBox4);
		this->Controls->Add(this->textBox3);
		this->Controls->Add(this->textBox2);
		this->Controls->Add(this->textBox1);
		this->Controls->Add(this->startb);
		this->Controls->Add(this->step);
		this->Controls->Add(this->label5);
		this->Controls->Add(this->commTextBox);
		this->Controls->Add(this->label4);
		this->Controls->Add(this->alphTextBox);
		this->Controls->Add(this->label3);
		this->Controls->Add(this->condTextBox);
		this->Controls->Add(this->label2);
		this->Controls->Add(this->label1);
		this->Controls->Add(this->menuStrip1);
		this->ForeColor = System::Drawing::Color::Black;
		this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
		this->HelpButton = true;
		this->helpProvider1->SetHelpNavigator(this, System::Windows::Forms::HelpNavigator::Topic);
		this->helpProvider1->SetHelpString(this, L"");
		this->MainMenuStrip = this->menuStrip1;
		this->MaximizeBox = false;
		this->MinimizeBox = false;
		this->Name = L"MyForm";
		this->helpProvider1->SetShowHelp(this, true);
		this->ShowIcon = false;
		this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
		this->Text = L" Turing Machine ";
		this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
		this->menuStrip1->ResumeLayout(false);
		this->menuStrip1->PerformLayout();
		this->ResumeLayout(false);
		this->PerformLayout();

	}

#pragma endregion

private: System::Void start_Click(System::Object^  sender, System::EventArgs^  e)
{
	if (start)
	{
		try
		{
			do
			{
				mt->Step();
				value = mt->GetStut();
				if (value < move)
					move--;
				if (value > txtbox->Length + move - 1)
					move++;
				ChangeStut();
				nowcond->Text = mt->GetCond();
				comm->Text = mt->GetPCom();
				mt->GetTape(txtbox, move);
			} while (!mt->GetSEnd());
			label9->Visible = true;
			timer->Visible = true;
			timer->Text = mt->GetTime() + "";
		}
		catch (int i)
		{
			if (i == 1)
				MessageBox::Show("Exceeding program runtime!");
		}
		start = false;
		startb->Enabled = false;
		step->Enabled = false;
		set->Focus();
	}
}
private: System::Void set_Click(System::Object^  sender, System::EventArgs^  e)
{	
	nowcond->Text = "";
	comm->Text = "";
	label9->Visible = false;
	timer->Visible = false;
	set->Focus();
	try
	{
		SetValues();
		start = true;
		startb->Enabled = true;
		step->Enabled = true;
	}
	catch (int i)
	{
		switch (i)
		{
		case 1:
			alphTextBox->ForeColor = cdw->colors[2];
			MessageBox::Show("Alphabet has invalidate data!");
			break;
		case 2:
			condTextBox->ForeColor = cdw->colors[2];
			MessageBox::Show("Condition list has invalidate data!");
			break;
		case 3:
			condTextBox->ForeColor = cdw->colors[2];
			MessageBox::Show("Condition list hasn't \"END\"!");
			break;
		case 4:
			commTextBox->ForeColor = cdw->colors[2];
			MessageBox::Show("Command list has invalidate data!");
			break;
		case 5:
			commTextBox->ForeColor = cdw->colors[2];
			MessageBox::Show("Command list hasn't \"END\"!");
			break;
		case 6:
			nowcond->ForeColor = cdw->colors[2];
			MessageBox::Show("First condition is \"END\"!");
			break;
		case 7:
			nowcond->ForeColor = cdw->colors[2];
			MessageBox::Show("First condition has invalidate data!");
			break;
		case 8:
			tapeTextBox->ForeColor = cdw->colors[2];
			MessageBox::Show("Tape list has invalidate data!");
			break;
		case 9:
			alphTextBox->ForeColor = cdw->colors[2];
			condTextBox->ForeColor = cdw->colors[2];
			MessageBox::Show("Alphabet & Condition list has common data!");
			break;
		}
	}
	catch (...)
	{
		MessageBox::Show("Wow, it`s error!");
	}
}
private: System::Void step_Click(System::Object^  sender, System::EventArgs^  e)
{
	if (start)
	{
		mt->Step();
		value = mt->GetStut();
		if (value < move)
			move--;
		if (value > txtbox->Length - 1 + move)
			move++;
		ChangeStut();
		mt->GetTape(txtbox, move);
		nowcond->Text = mt->GetCond();
		comm->Text = mt->GetPCom();
	}
	if (mt->GetEnd())
	{
		start = false;
		startb->Enabled = false;
		step->Enabled = false;
		label9->Visible = true;
		timer->Visible = true;
		timer->Text = mt->GetTime() + "";
		set->Focus();
	}
}
private: System::Void textBox1_Click(System::Object^  sender, System::EventArgs^  e) 
{
	set->Focus();	
	value = move;
	ChangeStut();
}
private: System::Void textBox2_Click(System::Object^  sender, System::EventArgs^  e)
{
	set->Focus();
	value = 1 + move;
	ChangeStut();
}
private: System::Void textBox3_Click(System::Object^  sender, System::EventArgs^  e) 
{
	set->Focus();
	value = 2 + move;
	ChangeStut();
}
private: System::Void textBox4_Click(System::Object^  sender, System::EventArgs^  e) 
{
	set->Focus();
	value = 3 + move;
	ChangeStut();
}
private: System::Void textBox5_Click(System::Object^  sender, System::EventArgs^  e)
{
	set->Focus();
	value = 4 + move;
	ChangeStut();
}
private: System::Void textBox6_Click(System::Object^  sender, System::EventArgs^  e)
{
	set->Focus();
	value = 5 + move;
	ChangeStut();
}
private: System::Void textBox7_Click(System::Object^  sender, System::EventArgs^  e) 
{
	set->Focus();
	value = 6 + move;
	ChangeStut();
}
private: System::Void textBox8_Click(System::Object^  sender, System::EventArgs^  e) 
{
	set->Focus();
	value = 7 + move;
	ChangeStut();
}
private: System::Void textBox9_Click(System::Object^  sender, System::EventArgs^  e) 
{
	set->Focus();
	value = 8 + move;
	ChangeStut();
}
private: System::Void textBox10_Click(System::Object^  sender, System::EventArgs^  e)
{
	set->Focus();
	value = 9 + move;
	ChangeStut();
}
private: System::Void textBox11_Click(System::Object^  sender, System::EventArgs^  e) 
{
	set->Focus();
	value = 10 + move;
	ChangeStut();
}
private: System::Void textBox12_Click(System::Object^  sender, System::EventArgs^  e) 
{
	set->Focus();
	value = 11 + move;
	ChangeStut();
}
private: System::Void textBox13_Click(System::Object^  sender, System::EventArgs^  e)
{
	set->Focus();
	value = 12 + move;
	ChangeStut();
}
private: System::Void textBox14_Click(System::Object^  sender, System::EventArgs^  e) 
{
	set->Focus();
	value = 13 + move;
	ChangeStut();
}
private: System::Void textBox15_Click(System::Object^  sender, System::EventArgs^  e) 
{
	set->Focus();
	value = 14 + move;
	ChangeStut();
}
private: System::Void textBox16_Click(System::Object^  sender, System::EventArgs^  e)
{
	set->Focus();
	value = 15 + move;
	ChangeStut();
}
private: System::Void textBox17_Click(System::Object^  sender, System::EventArgs^  e)
{
	set->Focus();
	value = 16 + move;
	ChangeStut();
}
private: System::Void textBox18_Click(System::Object^  sender, System::EventArgs^  e)
{
	set->Focus();
	value = 17 + move;
	ChangeStut();
}
private: System::Void left_Click(System::Object^  sender, System::EventArgs^  e) 
{
	move++;
	ChangeStut();
	mt->GetTape(txtbox, move);
}
private: System::Void right_Click(System::Object^  sender, System::EventArgs^  e) 
{
	move--;
	ChangeStut();
	mt->GetTape(txtbox, move);
}
private: System::Void textBox18_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	set->Focus();
}
private: System::Void newProgramToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
{
	TuringMachine::NewP^ d = gcnew TuringMachine::NewP();
	if (d->ShowDialog() == System::Windows::Forms::DialogResult::OK)
	{
		startb->Enabled = false;
		step->Enabled = false;
		value = 0;
		move = 0;
		fcond->Text = "";
		nowcond->Text = "";
		comm->Text = "";
		tapeTextBox->Text = "";
		condTextBox->Text = "END";
		alphTextBox->Text = L"λ";
		commTextBox->Text = "";
		commentTextBoxt->Text = "";
		label9->Visible = false;
		timer->Visible = false;
		for (int i = 0; i < txtbox->Length; i++)
		{
			txtbox[i]->Text = L"λ";
			txtbox[i]->BackColor = Color::White;
		}
		if (move + value >= 0 && move + value < txtbox->Length)
			txtbox[value + move]->BackColor = Color::PaleTurquoise;
		mt = gcnew MT(value);
		set->Focus();
		SetColors();
	}
}
private: System::Void saveProgramToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
{
	try
	{
		if (saveFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		{
			SetFields();
			mt->SaveToFile(mt, saveFileDialog1->FileName);
			set->Focus();
		}
	}
	catch (...)
	{
		MessageBox::Show("Saving failed!");
	}
}
private: System::Void loadProgramToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
{
	try
	{
		if (openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		{
			mt = gcnew MT();
			mt = mt->LoadFromFile(openFileDialog1->FileName);
			GetFields();
			set->Focus();
		}
	}
	catch (...)
	{
		MessageBox::Show("Load file is invalidate!");
	}
}
private: System::Void MyForm_Load(System::Object^  sender, System::EventArgs^  e) 
{
	helpProvider1->SetHelpString(alphTextBox, "The field for entering letters. Use one-character letters. Each letters is placed on a separate line. There must be no blank lines.");
	helpProvider1->SetHelpString(commTextBox, "The field for entering commands. Teams must consist of alphabet and states the instructions in the Related Fields. Teams should make an individual building sites. In the command list must contain at least one team to the state of completion of the work (\"END\"). There must be no blank lines.");
	helpProvider1->SetHelpString(condTextBox, "The field for entering conditions. The conditions should be written on separate lines. The list of conditions should contain a state shutdown. There must be no blank lines.");
	helpProvider1->SetHelpString(commentTextBoxt, "The field for entering comments.");
	helpProvider1->SetHelpString(tapeTextBox, "The field for entering the tape. Can contain only letters of the alphabet.");
	helpProvider1->SetHelpString(fcond, "The field for entering the first condition.");
	helpProvider1->SetHelpString(nowcond, "Field for the viewing present condition.");
	helpProvider1->SetHelpString(comm, "Field for the viewing perfomanse command.");
	helpProvider1->SetHelpString(timer, "Field for the viewing the number of passes.");
	helpProvider1->SetHelpString(label1, "Сommand that the program has just completed.");
	helpProvider1->SetHelpString(label2, "Condition from which the program starts.");
	helpProvider1->SetHelpString(label3, "The final set of states of the head (machine). One of the states should be the initial (run programs). Another one of the states (\"END\") must be finite (final program) - the stop state.");
	helpProvider1->SetHelpString(label4, "The final set whose elements are called letters.");
	helpProvider1->SetHelpString(label5, "The behavior of the machine (head) depending on the state and is considered a symbol.");
	helpProvider1->SetHelpString(label6, "Configuration endless a tape in both directions.");
	helpProvider1->SetHelpString(label7, "Your comments.");
	helpProvider1->SetHelpString(label8, "The condition in which the program is.");
	helpProvider1->SetHelpString(label9, "The number of passes made during program execution.");
	helpProvider1->SetHelpString(set, "Set the program.");
	helpProvider1->SetHelpString(step, "Make a step.");
	helpProvider1->SetHelpString(startb, "Start the program.");
	helpProvider1->SetHelpString(left, "Move tape left.");
	helpProvider1->SetHelpString(right, "Move tape right.");
	helpProvider1->SetHelpString(menuStrip1, "Menu.");
	helpProvider1->SetHelpString(menuStrip1, "Programm menu. You can create new program, save current program, load existing program, change decor or quit");
	for (int i = 0; i < txtbox->Length; i++)
		helpProvider1->SetHelpString(txtbox[i], "Cell Tape.");
	helpProvider1->SetHelpString(this, "Turing machine emulator.");
	set->Focus();
}
private: System::Void decorToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	if (cdw->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		SetColors();
}
private: System::Void quitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
{
	TuringMachine::Exit^ d = gcnew TuringMachine::Exit();
	if (d->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		Application::Exit();
}
private: System::Void aboutTheProgramToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	TuringMachine::About^ d = gcnew TuringMachine::About();
	d->ShowDialog();
}
private: System::Void toolStripMenuItem2_Click(System::Object^  sender, System::EventArgs^  e) 
{
	toolStripMenuItem2->Checked = true;
	toolStripMenuItem3->Checked = false;
	toolStripMenuItem4->Checked = false;
	mt->SetLimit(500);
}
private: System::Void toolStripMenuItem3_Click(System::Object^  sender, System::EventArgs^  e) 
{
	toolStripMenuItem2->Checked = false;
	toolStripMenuItem3->Checked = true;
	toolStripMenuItem4->Checked = false;
	mt->SetLimit(5000);
}
private: System::Void toolStripMenuItem4_Click(System::Object^  sender, System::EventArgs^  e)
{
	toolStripMenuItem2->Checked = false;
	toolStripMenuItem3->Checked = false;
	toolStripMenuItem4->Checked = true;
	mt->SetLimit(50000);
}
private: void SetValues()
{
	SetColors();
	mt->AddAlphList(alphTextBox->Text);
	mt->SetTape(tapeTextBox->Text);
	mt->GetTape(txtbox, move);
	mt->AddCondList(condTextBox->Text);
	mt->AddCommList(commTextBox->Text);
	mt->SetStut(value);
	mt->SetCond(fcond->Text);
	mt->NewStart();
}
private: void ChangeStut()
{
	for (int i = 0; i < txtbox->Length; i++)
	{
		txtbox[i]->BackColor = Color::White;
	}
	if (value - move >= 0 && value - move < txtbox->Length)
		txtbox[value - move]->BackColor = cdw->colors[1];
}
private: void SetFields()
{
	array<String^>^ s = gcnew array<String^>(5);
	s[0] = alphTextBox->Text;
	s[1] = condTextBox->Text;
	s[2] = commTextBox->Text;
	s[3] = tapeTextBox->Text;
	s[4] = commentTextBoxt->Text;
	mt->SetFields(s);
}
private: void GetFields()
{
	array<String^>^ s = mt->GetFields();
	alphTextBox->Text = s[0];
	condTextBox->Text = s[1];
	commTextBox->Text = s[2];
	tapeTextBox->Text = s[3];
	commentTextBoxt->Text = s[4];
	fcond->Text = mt->GetCond();
	value = mt->GetStut();
	nowcond->Text = "";
	comm->Text = "";
	label9->Visible = false;
	timer->Visible = false;
	SetColors();
	switch (mt->GetLimit())
	{
		toolStripMenuItem2->Checked = false;	
		toolStripMenuItem3->Checked = false;	
		toolStripMenuItem4->Checked = false;
	case 500: 	
		toolStripMenuItem2->Checked = true;
		break;
	case 5000:
		toolStripMenuItem3->Checked = true;
		break;
	case 50000:
		toolStripMenuItem4->Checked = true;
		break;
	default: 
		toolStripMenuItem3->Checked = true;
		mt->SetLimit(5000);
		break;
	}
}
private: void SetColors()
{
	mt->SetStut(value);
	alphTextBox->ForeColor = cdw->colors[0];
	condTextBox->ForeColor = cdw->colors[0];
	commTextBox->ForeColor = cdw->colors[0];
	nowcond->ForeColor = cdw->colors[0];
	commTextBox->ForeColor = cdw->colors[0];
	tapeTextBox->ForeColor = cdw->colors[0];
	fcond->ForeColor = cdw->colors[0];
	nowcond->ForeColor = cdw->colors[0];
	comm->ForeColor = cdw->colors[0];
	timer->ForeColor = cdw->colors[0];
	label1->ForeColor = cdw->colors[0];
	label2->ForeColor = cdw->colors[0];
	label3->ForeColor = cdw->colors[0];
	label4->ForeColor = cdw->colors[0];
	label5->ForeColor = cdw->colors[0];
	label6->ForeColor = cdw->colors[0];
	label7->ForeColor = cdw->colors[0];
	label8->ForeColor = cdw->colors[0];
	label9->ForeColor = cdw->colors[0];
	set->ForeColor = cdw->colors[0];
	step->ForeColor = cdw->colors[0];
	startb->ForeColor = cdw->colors[0];
	left->ForeColor = cdw->colors[0];
	right->ForeColor = cdw->colors[0];
	programToolStripMenuItem->ForeColor = cdw->colors[0];
	optionsToolStripMenuItem->ForeColor = cdw->colors[0];
	quitToolStripMenuItem->ForeColor = cdw->colors[0];
	newProgramToolStripMenuItem1->ForeColor = cdw->colors[0];
	saveProgramToolStripMenuItem1->ForeColor = cdw->colors[0];
	loadProgramToolStripMenuItem1->ForeColor = cdw->colors[0];
	decorToolStripMenuItem->ForeColor = cdw->colors[0];
	executionLimitToolStripMenuItem->ForeColor = cdw->colors[0];
	aboutTheProgramToolStripMenuItem->ForeColor = cdw->colors[0];
	toolStripMenuItem2->ForeColor = cdw->colors[0];
	toolStripMenuItem3->ForeColor = cdw->colors[0];
	toolStripMenuItem4->ForeColor = cdw->colors[0];
	menuStrip1->BackColor = cdw->colors[3];
	this->BackColor = cdw->colors[4];
	for (int i = 0; i < txtbox->Length; i++)
		txtbox[i]->ForeColor = cdw->colors[0];
	ChangeStut();
}
};
}