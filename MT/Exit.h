#pragma once

namespace TuringMachine {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class Exit : public System::Windows::Forms::Form
	{
	public:
		Exit(void)
		{
			InitializeComponent();
		}
	protected:
		~Exit()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Label^  label1;
	private: System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code

			 void InitializeComponent(void)
			 {
				 this->button2 = (gcnew System::Windows::Forms::Button());
				 this->button1 = (gcnew System::Windows::Forms::Button());
				 this->label1 = (gcnew System::Windows::Forms::Label());
				 this->SuspendLayout();
				 // 
				 // button2
				 // 
				 this->button2->BackColor = System::Drawing::Color::White;
				 this->button2->Cursor = System::Windows::Forms::Cursors::Hand;
				 this->button2->FlatAppearance->BorderColor = System::Drawing::Color::White;
				 this->button2->FlatAppearance->BorderSize = 0;
				 this->button2->FlatAppearance->MouseDownBackColor = System::Drawing::Color::DarkGray;
				 this->button2->FlatAppearance->MouseOverBackColor = System::Drawing::Color::Gainsboro;
				 this->button2->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
				 this->button2->Font = (gcnew System::Drawing::Font(L"Georgia", 12));
				 this->button2->ImeMode = System::Windows::Forms::ImeMode::NoControl;
				 this->button2->Location = System::Drawing::Point(167, 73);
				 this->button2->Margin = System::Windows::Forms::Padding(2, 3, 2, 3);
				 this->button2->Name = L"button2";
				 this->button2->Size = System::Drawing::Size(155, 35);
				 this->button2->TabIndex = 8;
				 this->button2->Text = L"Cansel";
				 this->button2->UseVisualStyleBackColor = false;
				 this->button2->Click += gcnew System::EventHandler(this, &Exit::button2_Click);
				 // 
				 // button1
				 // 
				 this->button1->BackColor = System::Drawing::Color::White;
				 this->button1->Cursor = System::Windows::Forms::Cursors::Hand;
				 this->button1->FlatAppearance->BorderColor = System::Drawing::Color::White;
				 this->button1->FlatAppearance->BorderSize = 0;
				 this->button1->FlatAppearance->MouseDownBackColor = System::Drawing::Color::DarkGray;
				 this->button1->FlatAppearance->MouseOverBackColor = System::Drawing::Color::Gainsboro;
				 this->button1->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
				 this->button1->Font = (gcnew System::Drawing::Font(L"Georgia", 12));
				 this->button1->ImeMode = System::Windows::Forms::ImeMode::NoControl;
				 this->button1->Location = System::Drawing::Point(10, 73);
				 this->button1->Margin = System::Windows::Forms::Padding(2, 3, 2, 3);
				 this->button1->Name = L"button1";
				 this->button1->Size = System::Drawing::Size(155, 35);
				 this->button1->TabIndex = 7;
				 this->button1->Text = L"Exit";
				 this->button1->UseVisualStyleBackColor = false;
				 this->button1->Click += gcnew System::EventHandler(this, &Exit::button1_Click);
				 // 
				 // label1
				 // 
				 this->label1->BackColor = System::Drawing::Color::White;
				 this->label1->Font = (gcnew System::Drawing::Font(L"Georgia", 12));
				 this->label1->ImeMode = System::Windows::Forms::ImeMode::NoControl;
				 this->label1->Location = System::Drawing::Point(10, 8);
				 this->label1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
				 this->label1->Name = L"label1";
				 this->label1->Size = System::Drawing::Size(315, 62);
				 this->label1->TabIndex = 6;
				 this->label1->Text = L"Quit, all unsaved data will be lost\?";
				 this->label1->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
				 // 
				 // Exit
				 // 
				 this->AutoScaleDimensions = System::Drawing::SizeF(7, 15);
				 this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				 this->BackColor = System::Drawing::Color::White;
				 this->ClientSize = System::Drawing::Size(333, 114);
				 this->ControlBox = false;
				 this->Controls->Add(this->button2);
				 this->Controls->Add(this->button1);
				 this->Controls->Add(this->label1);
				 this->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
					 static_cast<System::Byte>(204)));
				 this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
				 this->MaximizeBox = false;
				 this->MinimizeBox = false;
				 this->Name = L"Exit";
				 this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
				 this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
				 this->Text = L"Exit";
				 this->ResumeLayout(false);

			 }

#pragma endregion

	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e)
	{
		this->DialogResult = System::Windows::Forms::DialogResult::OK;
		this->Close();
	}
	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e)
	{
		this->DialogResult = System::Windows::Forms::DialogResult::Cancel;
		this->Close();
	}
	};
}
