#ifndef MT_h
#define MT_h

#include < stdio.h >
#include < stdlib.h >
#include < vcclr.h >

using namespace System;
using namespace System::Windows::Forms;
using namespace System::Runtime::Serialization;
using namespace System::IO;
using namespace System::Runtime::Serialization::Formatters::Binary;

[Serializable]

ref struct Tape
{
public:
	wchar_t value;
	int nomber;
	Tape^ next;
	Tape^ prev;
	Tape() {}
	Tape(wchar_t v)
	{
		nomber = 0;
		value = v;
		next = nullptr;
		prev = nullptr;
	}
	Tape(wchar_t v, Tape^ p, int n)
	{
		nomber = n;
		value = v;
		next = nullptr;
		prev = p;
		p->next = this;
	}
	Tape(wchar_t v, int n, Tape^ p)
	{
		nomber = n;
		value = v;
		prev = nullptr;
		next = p;
		p->prev = this;
	}
};

[Serializable]

ref struct Command
{
public:
	wchar_t value;
	wchar_t next_value;
	String^ next_cond;
	String^ oper;
	Command^ next;
	Command() {};
	Command(wchar_t v, wchar_t nv, String^ nc, String^ op)
	{
		value = v;
		next_value = nv;
		next_cond = nc;
		next = nullptr;
		oper = op;
	}
};

[Serializable]

ref struct Condition
{
public:
	String^ name;
	Command^ com;
	Condition^ next;
	Condition() {};
	Condition(String^ s)
	{
		name = s;
		com = nullptr;
		next = nullptr;
	}
	void AddComm(wchar_t v, wchar_t nv, String^ nc, String^ op)
	{
		if (com == nullptr)
			com = gcnew Command(v, nv, nc, op);
		else
		{
			Command^ c = com;
			
			while (c->next != nullptr)
			{
				if (c->value == v)
					throw 4;
				c = c->next;
			}
			if (c->value == v)
					throw 4;
			c->next = gcnew Command(v, nv, nc, op);
		}
	}
	Command^ GetComm(wchar_t s)
	{
		Command^ c = com;
		if (c != nullptr)
		{
			while (c->next != nullptr && c->value != s)
				c = c->next;
			if (c->value == s)
				return c;
			else
				return nullptr;
		}
		else
			return nullptr;
	}
};

[Serializable]

ref class MT
{
public:
	MT();
	MT(MT^);
	MT(int);
	void AddCommList(String^);
	void AddAlphList(String^);
	void AddCondList(String^);
	void SetTape(String^);
	void GetTape(array<TextBox^>^, int);
	void SetStut(int);
	int GetStut();
	void SetLimit(int);
	int GetLimit();
	int GetTime();
	bool GetEnd();
	bool GetSEnd();
	void SetCond(String^);
	String^ GetCond();
	String^ GetPCom();
	void Step();
	void NewStart();
	void SaveToFile(Object^, String^);
	MT^ LoadFromFile(String^);
	void SetFields(array<String^>^);
	array<String^>^ GetFields();
private:
	int time;
	int limit;
	bool end;
	int stut;
	String^ pcom;
	String^ condition;
	Tape^ tape;
	Condition^ cond;
	array<String^>^ alphabet;
	array<String^>^ fields;
};

#endif