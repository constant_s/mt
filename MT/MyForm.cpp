#include "MyForm.h"

using namespace System;
using namespace System::Windows::Forms;

[STAThread]
void main(array<String^>^ arg) 
{
	try
	{
		Application::EnableVisualStyles();
		Application::SetCompatibleTextRenderingDefault(false);
		TuringMachine::MyForm form; 
		Application::Run(%form);
	}
	catch (...)
	{
		MessageBox::Show("Turing, we have a problem!");
	}
}